#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_pre, cycle_length

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # pre
    # ----

    def test_pre(self):
        a = collatz_pre(1000003)
        self.assertEqual(a, 1000003)
    
    def test_pre2(self):
        a = collatz_pre(1000004)
        self.assertEqual(a, 1000004)
            
    def test_pre3(self):
        a = collatz_pre(1000005)
        self.assertEqual(a, 1000005)
    
    # ----
    # read
    # ----

    
    def test_read(self):
        s = "1 1000000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 1000000)

    def test_read2(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)
        
    def test_read3(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        collatz_eval(i,j)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)
        
    # ----
    # cycle_length
    # ----    
        
        
    def test_cycle_length(self):
        v = cycle_length(1)
        self.assertEqual(v, 1)
    
    def test_cycle_length2(self):
        v = cycle_length(10)
        self.assertEqual(v, 7)
      
    def test_cycle_length3(self):
        v = cycle_length(1000000)
        self.assertEqual(v, 153)
   
    
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(762818, 763068)
        self.assertEqual(v, 269)
        
    def test_eval_6(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)
        
    def test_eval_7(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)
        
    def test_eval_8(self):
        v = collatz_eval(10, 10)
        self.assertEqual(v, 7)    
    
    def test_eval_9(self):
        v = collatz_eval(8, 10)
        self.assertEqual(v, 20)
        
    def test_eval_10(self):
        v = collatz_eval(1, 1000002)
        self.assertEqual(v, 525) 
    
    
    
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
        
    def test_print2(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")
        
    def test_print3(self):
        w = StringIO()
        collatz_print(w, 20, 10, 1)
        self.assertEqual(w.getvalue(), "20 10 1\n")    
    

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve2(self):
        r = StringIO("10 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n")
    
    def test_solve3(self):
        r = StringIO("1 1\n\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n")
            

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.........................
----------------------------------------------------------------------
Ran 25 tests in 7.689s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.........................
----------------------------------------------------------------------
Ran 25 tests in 7.689s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          49      0     24      0   100%
TestCollatz.py      97      0      0      0   100%
------------------------------------------------------------
TOTAL              146      0     24      0   100%
"""
