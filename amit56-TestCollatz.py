#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "999999 999999\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 999999)
        self.assertEqual(j, 999999)

    def test_read_3(self):
        s = "34908 938391"
        i, j = collatz_read(s)
        self.assertEqual(i, 34908)
        self.assertEqual(j, 938391)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(724, 724)
        self.assertEqual(v, 21)

    def test_eval_7(self):
        v = collatz_eval(729, 917)
        self.assertEqual(v, 179)

    def test_eval_8(self):
        v = collatz_eval(223, 21724)
        self.assertEqual(v, 279)

    def test_eval_9(self):
        v = collatz_eval(1001, 3000)
        self.assertEqual(v, 217)

    def test_eval_10(self):
        v = collatz_eval(6400, 6600)
        self.assertEqual(v, 244)

    def test_eval_11(self):
        v = collatz_eval(84999, 87000)
        self.assertEqual(v, 302)

    def test_eval_12(self):
        v = collatz_eval(6500, 9001)
        self.assertEqual(v, 257)

    def test_eval_13(self):
        v = collatz_eval(7000, 8999)
        self.assertEqual(v, 252)

    def test_eval_14(self):
        v = collatz_eval(60000, 81000)
        self.assertEqual(v, 351)

    '''
    # failure case, I confirmed the expected failure

    def test_eval_failure_case(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 1)

    '''
    


    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 56921, 86463, 351)
        self.assertEqual(w.getvalue(), "56921 86463 351\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 88, 88, 18)
        self.assertEqual(w.getvalue(), "88 88 18\n")



    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("93453 99382\n23425 33355\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "93453 99382 328\n23425 33355 308\n")

    def test_solve_3(self):
        r = StringIO("1 1\n 10 10\n 100 100\n 110 111\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n10 10 7\n100 100 26\n110 111 114\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
