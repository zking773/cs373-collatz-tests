#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 201)
        self.assertEqual(j, 210)

    # ----
    # eval
    # ----

    def test_eval_01(self):
        v = collatz_eval(566011, 494361)
        self.assertEqual(v, 470)

    def test_eval_02(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_03(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_04(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_05(self):
        v = collatz_eval(10, 3)
        self.assertEqual(v, 20)

    def test_eval_06(self):
        v = collatz_eval(5, 5)
        self.assertEqual(v, 6)

    def test_eval_07(self):
        v = collatz_eval(100, 30)
        self.assertEqual(v, 119)

    def test_eval_08(self):
        v = collatz_eval(1000, 1000)
        self.assertEqual(v, 112)

    def test_eval_09(self):
        v = collatz_eval(2531, 1215)
        self.assertEqual(v, 209)

    def test_eval_10(self):
        v = collatz_eval(345, 600)
        self.assertEqual(v, 142)

    def test_eval_11(self):
        v = collatz_eval(500, 250)
        self.assertEqual(v, 144)

    def test_eval_12(self):
        v = collatz_eval(500000, 999999)
        self.assertEqual(v, 525)


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 150, 122)
        self.assertEqual(w.getvalue(), "100 150 122\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 500, 250, 144)
        self.assertEqual(w.getvalue(), "500 250 144\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    
    def test_solve_2(self):
        r = StringIO("10 3\n5 5\n100 30\n1000 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 3 20\n5 5 6\n100 30 119\n1000 1000 112\n")

    def test_solve_3(self):
        r = StringIO("2531 1215\n345 600\n500 250\n100 150\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2531 1215 209\n345 600 142\n500 250 144\n100 150 122\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
