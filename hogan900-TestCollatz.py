#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_1(self):
        s = "324 745\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 324)
        self.assertEqual(j, 745)

    def test_read_2(self):
        s = "5000 3000\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5000)
        self.assertEqual(j, 3000)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(1000, 2000)
        self.assertEqual(v, 182)

    def test_eval_3(self):
        v = collatz_eval(210, 201)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(623, 21345)
        self.assertEqual(v, 279)
    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 20, 20, 8)
        self.assertEqual(w.getvalue(), "20 20 8\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 786821, 250481, 509)
        self.assertEqual(w.getvalue(), "786821 250481 509\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_1(self):
        r = StringIO("5 5\n10 10\n20 20\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "5 5 6\n10 10 7\n20 20 8\n")

    def test_solve_2(self):
        r = StringIO("86417 272962\n\n455645 528452\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "86417 272962 443\n455645 528452 470\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""

